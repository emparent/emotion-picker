# emotionpicker

A new Flutter package project.

## Getting Started

To make this work, you have to:
- Add I18n.delegate to your localizationsDelegates in your main app widget
- Add Emotion Picker to dependencies in pubspec.yaml
- Add Emotion Icons as a font to your pubspec.yaml file ( asset: packages/emotionpicker/fonts/EmotionIcons.ttf )

### Adding the Emotion Icons font
Add the following lines to pubspec.yaml under fonts:

- family: Emotion Icons
      fonts:
          - asset: packages/emotionpicker/fonts/EmotionIcons.ttf