// Copyright (c) 2019 Miroslav Mazel
// 
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import 'package:flutter/widgets.dart';

class EmotionDisplay {
  static const List<int> basicColorsLight = [
    0xff57E389,
    0xff33D1D1,
    0xff838287,
    0xff99C1F1,
    0xffC061CB,
    0xffED333B,
    0xffF6F5F4,
    0xffF6D32D
  ];

  static const List<int> basicColors = [
    0xff57E389,
    0xff00FFFF,
    0xff000000,
    0xff3584E4,
    0xffC061CB,
    0xffED333B,
    0xffFFFFFF,
    0xffF9F06B
  ];

  static const List<int> fontCodePoints = [
    0xe805,
    0xe807,
    0xe803,
    0xe801,
    0xe802,
    0xe800,
    0xe806,
    0xe804
  ];

  static const String _FONT = "Emotion Icons";

  static getIconData(int emotionPos) {
    return IconData(fontCodePoints[emotionPos], fontFamily: _FONT);
  }
}
