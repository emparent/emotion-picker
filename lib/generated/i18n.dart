import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
// ignore_for_file: non_constant_identifier_names
// ignore_for_file: camel_case_types
// ignore_for_file: prefer_single_quotes
// ignore_for_file: unnecessary_brace_in_string_interps

//WARNING: This file is automatically generated. DO NOT EDIT, all your changes would be lost.

typedef void LocaleChangeCallback(Locale locale);

class I18n implements WidgetsLocalizations {
  const I18n();
  static Locale _locale;
  static bool _shouldReload = false;

  static set locale(Locale _newLocale) {
    _shouldReload = true;
    I18n._locale = _newLocale;
  }

  static const GeneratedLocalizationsDelegate delegate =
    const GeneratedLocalizationsDelegate();

  /// function to be invoked when changing the language
  static LocaleChangeCallback onLocaleChanged;

  static I18n of(BuildContext context) =>
    Localizations.of<I18n>(context, WidgetsLocalizations);

  @override
  TextDirection get textDirection => TextDirection.ltr;

  /// "Drag an emoji to choose an emotion"
  String get dragTooltip => "Drag an emoji to choose an emotion";
  /// "bored"
  String get bored => "bored";
  /// "apathetic"
  String get apathetic => "apathetic";
  /// "repulsed"
  String get repulsed => "repulsed";
  /// "bitter"
  String get bitter => "bitter";
  /// "disgusted"
  String get disgusted => "disgusted";
  /// "contemptuous"
  String get contemptuous => "contemptuous";
  /// "loathful"
  String get loathful => "loathful";
  /// "unfocused"
  String get unfocused => "unfocused";
  /// "distracted"
  String get distracted => "distracted";
  /// "surprised"
  String get surprised => "surprised";
  /// "amazed"
  String get amazed => "amazed";
  /// "aghast"
  String get aghast => "aghast";
  /// "shocked"
  String get shocked => "shocked";
  /// "awestruck"
  String get awestruck => "awestruck";
  /// "vulnerable"
  String get vulnerable => "vulnerable";
  /// "flustered"
  String get flustered => "flustered";
  /// "embarassed"
  String get embarassed => "embarassed";
  /// "inferior"
  String get inferior => "inferior";
  /// "regretful"
  String get regretful => "regretful";
  /// "guilty"
  String get guilty => "guilty";
  /// "ashamed"
  String get ashamed => "ashamed";
  /// "pensive"
  String get pensive => "pensive";
  /// "down"
  String get down => "down";
  /// "sad"
  String get sad => "sad";
  /// "mournful"
  String get mournful => "mournful";
  /// "sorrowful"
  String get sorrowful => "sorrowful";
  /// "miserable"
  String get miserable => "miserable";
  /// "anguished"
  String get anguished => "anguished";
  /// "peeved"
  String get peeved => "peeved";
  /// "irritated"
  String get irritated => "irritated";
  /// "upset"
  String get upset => "upset";
  /// "angry"
  String get angry => "angry";
  /// "furious"
  String get furious => "furious";
  /// "livid"
  String get livid => "livid";
  /// "enraged"
  String get enraged => "enraged";
  /// "jittery"
  String get jittery => "jittery";
  /// "concerned"
  String get concerned => "concerned";
  /// "nervous"
  String get nervous => "nervous";
  /// "worried"
  String get worried => "worried";
  /// "scared"
  String get scared => "scared";
  /// "anxious"
  String get anxious => "anxious";
  /// "terrified"
  String get terrified => "terrified";
  /// "lively"
  String get lively => "lively";
  /// "interested"
  String get interested => "interested";
  /// "focused"
  String get focused => "focused";
  /// "inspired"
  String get inspired => "inspired";
  /// "enthusiastic"
  String get enthusiastic => "enthusiastic";
  /// "excited"
  String get excited => "excited";
  /// "exhilirated"
  String get exhilirated => "exhilirated";
  /// "relaxed"
  String get relaxed => "relaxed";
  /// "calm"
  String get calm => "calm";
  /// "pleasant"
  String get pleasant => "pleasant";
  /// "content"
  String get content => "content";
  /// "joyful"
  String get joyful => "joyful";
  /// "happy"
  String get happy => "happy";
  /// "blissful"
  String get blissful => "blissful";
}

class _I18n_en_US extends I18n {
  const _I18n_en_US();

  @override
  TextDirection get textDirection => TextDirection.ltr;
}

class _I18n_cs_CZ extends I18n {
  const _I18n_cs_CZ();

  /// "Táhněte kolečkem, abyste vybral/a emoci"
  @override
  String get dragTooltip => "Táhněte kolečkem, abyste vybral/a emoci";
  /// "znuděně"
  @override
  String get bored => "znuděně";
  /// "apaticky"
  @override
  String get apathetic => "apaticky";
  /// "odrazeně"
  @override
  String get repulsed => "odrazeně";
  /// "zahořkle"
  @override
  String get bitter => "zahořkle";
  /// "znechuceně"
  @override
  String get disgusted => "znechuceně";
  /// "pohrdavě"
  @override
  String get contemptuous => "pohrdavě";
  /// "opovržlivě"
  @override
  String get loathful => "opovržlivě";
  /// "nesoustředěně"
  @override
  String get unfocused => "nesoustředěně";
  /// "rozptýleně"
  @override
  String get distracted => "rozptýleně";
  /// "překvapeně"
  @override
  String get surprised => "překvapeně";
  /// "ohromeně"
  @override
  String get amazed => "ohromeně";
  /// "udiveně"
  @override
  String get aghast => "udiveně";
  /// "šokovaně"
  @override
  String get shocked => "šokovaně";
  /// "užasle"
  @override
  String get awestruck => "užasle";
  /// "zranitelně"
  @override
  String get vulnerable => "zranitelně";
  /// "rozpačitě"
  @override
  String get flustered => "rozpačitě";
  /// "trapně"
  @override
  String get embarassed => "trapně";
  /// "podřadně"
  @override
  String get inferior => "podřadně";
  /// "kajícně"
  @override
  String get regretful => "kajícně";
  /// "provinile"
  @override
  String get guilty => "provinile";
  /// "zahanbeně"
  @override
  String get ashamed => "zahanbeně";
  /// "zádumčivě"
  @override
  String get pensive => "zádumčivě";
  /// "zklesle"
  @override
  String get down => "zklesle";
  /// "smutně"
  @override
  String get sad => "smutně";
  /// "truchlivě"
  @override
  String get mournful => "truchlivě";
  /// "zarmouceně"
  @override
  String get sorrowful => "zarmouceně";
  /// "uboze"
  @override
  String get miserable => "uboze";
  /// "zoufale"
  @override
  String get anguished => "zoufale";
  /// "podrážděně"
  @override
  String get peeved => "podrážděně";
  /// "vytočeně"
  @override
  String get irritated => "vytočeně";
  /// "pobouřeně"
  @override
  String get upset => "pobouřeně";
  /// "rozčileně"
  @override
  String get angry => "rozčileně";
  /// "rozzlobeně"
  @override
  String get furious => "rozzlobeně";
  /// "rozhněvaně"
  @override
  String get livid => "rozhněvaně";
  /// "rozzuřeně"
  @override
  String get enraged => "rozzuřeně";
  /// "znervózněně"
  @override
  String get jittery => "znervózněně";
  /// "znepokojeně"
  @override
  String get concerned => "znepokojeně";
  /// "nervózně"
  @override
  String get nervous => "nervózně";
  /// "ustaraně"
  @override
  String get worried => "ustaraně";
  /// "vystrašeně"
  @override
  String get scared => "vystrašeně";
  /// "úzkostlivě"
  @override
  String get anxious => "úzkostlivě";
  /// "zděšeně"
  @override
  String get terrified => "zděšeně";
  /// "čile"
  @override
  String get lively => "čile";
  /// "zaujatě"
  @override
  String get interested => "zaujatě";
  /// "soustředěně"
  @override
  String get focused => "soustředěně";
  /// "inspirovaně"
  @override
  String get inspired => "inspirovaně";
  /// "nadšeně"
  @override
  String get enthusiastic => "nadšeně";
  /// "vzrušeně"
  @override
  String get excited => "vzrušeně";
  /// "zapáleně"
  @override
  String get exhilirated => "zapáleně";
  /// "uvolněně"
  @override
  String get relaxed => "uvolněně";
  /// "uklidněně"
  @override
  String get calm => "uklidněně";
  /// "příjemně"
  @override
  String get pleasant => "příjemně";
  /// "spokojeně"
  @override
  String get content => "spokojeně";
  /// "potěšěně"
  @override
  String get joyful => "potěšěně";
  /// "šťastně"
  @override
  String get happy => "šťastně";
  /// "blaženě"
  @override
  String get blissful => "blaženě";

  @override
  TextDirection get textDirection => TextDirection.ltr;
}

class GeneratedLocalizationsDelegate extends LocalizationsDelegate<WidgetsLocalizations> {
  const GeneratedLocalizationsDelegate();
  List<Locale> get supportedLocales {
    return const <Locale>[
      const Locale("en", "US"),
      const Locale("cs", "CZ")
    ];
  }

  LocaleResolutionCallback resolution({Locale fallback}) {
    return (Locale locale, Iterable<Locale> supported) {
      if (this.isSupported(locale)) {
        return locale;
      }
      final Locale fallbackLocale = fallback ?? supported.first;
      return fallbackLocale;
    };
  }

  @override
  Future<WidgetsLocalizations> load(Locale _locale) {
    I18n._locale ??= _locale;
    I18n._shouldReload = false;
    final Locale locale = I18n._locale;
    final String lang = locale != null ? locale.toString() : "";
    final String languageCode = locale != null ? locale.languageCode : "";
    if ("en_US" == lang) {
      return new SynchronousFuture<WidgetsLocalizations>(const _I18n_en_US());
    }
    else if ("cs_CZ" == lang) {
      return new SynchronousFuture<WidgetsLocalizations>(const _I18n_cs_CZ());
    }
    else if ("en" == languageCode) {
      return new SynchronousFuture<WidgetsLocalizations>(const _I18n_en_US());
    }
    else if ("cs" == languageCode) {
      return new SynchronousFuture<WidgetsLocalizations>(const _I18n_cs_CZ());
    }

    return new SynchronousFuture<WidgetsLocalizations>(const I18n());
  }

  @override
  bool isSupported(Locale locale) {
    for (var i = 0; i < supportedLocales.length && locale != null; i++) {
      final l = supportedLocales[i];
      if (l.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }

  @override
  bool shouldReload(GeneratedLocalizationsDelegate old) => I18n._shouldReload;
}