// Copyright (c) 2019 Miroslav Mazel
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//import 'package:custom_shape/curve_painter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'dart:core';
import 'dart:math';
import 'emotion.dart';
import 'emotion_display.dart';
import 'generated/i18n.dart';
import 'src/emotion_shapes.dart';

class EmotionCanvas extends StatefulWidget {
  final Function(Emotion) onValueSet;
  final Function(Emotion) onChanged;

  EmotionCanvas({Key key, @required this.onValueSet, this.onChanged})
      : super(key: key);

  @override
  EmotionCanvasState createState() => EmotionCanvasState();
}

class EmotionPainter extends CustomPainter {
  int emotionIndex;
  double intensityDist;

  EmotionPainter({this.emotionIndex, this.intensityDist});

  @override
  void paint(Canvas canvas, Size size) {
    if (emotionIndex != null && intensityDist != null) {
      Emotions.leaf(canvas, size, emotionIndex);
    } else {
      Emotions.background(canvas, size);
    }
  }

  @override
  bool shouldRepaint(EmotionPainter oldDelegate) {
    return oldDelegate.emotionIndex != emotionIndex ||
        oldDelegate.intensityDist != intensityDist;
  }
}

class EmotionAngleHelper {
  static const double _EMOT_ARC = 2 * pi / Emotion.EMOTIONCOUNT;
  static double getEmotionAngleRads(int emotionIndex) {
    return emotionIndex * _EMOT_ARC;
  }
}

class DragConstants {
  Emotion emotion;
  bool horizDrag;
  double dragDirectionCosSin;

  DragConstants(this.emotion, this.horizDrag) {
    double angle = EmotionAngleHelper.getEmotionAngleRads(emotion.emotion);
    dragDirectionCosSin = horizDrag ? cos(angle) : sin(angle);
  }
}

class EmotionCanvasState extends State<EmotionCanvas> {
  DragConstants _dragConstants;
  double _sideLen = 0;
  Offset _boxCenter;

  @override
  Widget build(BuildContext context) {
    // todo _boxCenter = Offset(renderBox.size.width / 2, renderBox.size.height / 2);
    Size availSize = MediaQuery.of(context).size;
    _sideLen =
        availSize.width < availSize.height ? availSize.width : availSize.height;
    _boxCenter = Offset(_sideLen / 2, _sideLen / 2);
    double _fragmentLen = (_sideLen / 2) /
        Emotion.INTENSITYCOUNT *
        0.82; //0.82 compensates for icon at the end

    return AspectRatio(
      // todo make room for cancel!
      aspectRatio: 1,
      child: CustomPaint(
        painter: EmotionPainter(
            emotionIndex: _dragConstants?.emotion?.emotion,
            intensityDist: _getIntensityDistance(_fragmentLen)),
        child: Stack(
          children: List.generate(Emotion.EMOTIONCOUNT, (i) {
            var visible = (_dragConstants == null ||
                _dragConstants?.emotion?.emotion == i);
            return Visibility(
                visible: visible,
                child: Align(
                    alignment:
                        _getHandleOffset(i, visible ? _dragConstants : null),
                    child: GestureDetector(
                      child: EmotionHandle(EmotionDisplay.getIconData(i),
                          Color(EmotionDisplay.basicColorsLight[i])),
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text(I18n.of(context)
                                .dragTooltip))); //todo create a better indication later
                      },
                      onPanStart: _onPanStart,
                      onPanUpdate: _onPanUpdate,
                      onPanEnd: _onPanEnd,
                    )));
          }),
        ),
      ),
    );
  }

  void _onPanStart(DragStartDetails details) {
    RenderBox renderBox = context.findRenderObject();
    Offset localPos = renderBox.globalToLocal(details.globalPosition);

    setState(() {
      //todo don't really need the arc here AT ALL
      int emotionNum = _getEmotion(localPos);
      _dragConstants = DragConstants(
          Emotion(emotion: emotionNum, intensity: 0),
          (EmotionAngleHelper.getEmotionAngleRads(emotionNum) + pi / 4) % pi <=
              pi / 2);
    }); //todo can't I use offset instead?

    if (widget.onChanged != null) widget.onChanged(_dragConstants.emotion);
  }

  void _onPanUpdate(DragUpdateDetails details) {
    RenderBox renderBox = context.findRenderObject();

    var height = renderBox.size.height;
    var y = renderBox.globalToLocal(details.globalPosition).dy;

    if (y > height) {
      if (_dragConstants.emotion.intensity != null) {
        setState(() {
          _dragConstants.emotion.intensity = null;
          if (widget.onChanged != null)
            widget.onChanged(_dragConstants.emotion);
        });
      }
    } else {
      var width = renderBox.size.width;

      double axisPos = _dragConstants.horizDrag
          ? renderBox.globalToLocal(details.globalPosition).dx
          : y;
      double axisDim = _dragConstants.horizDrag
          ? width * _dragConstants.dragDirectionCosSin
          : height * _dragConstants.dragDirectionCosSin;
      double offset = _dragConstants.horizDrag
          ? (width - axisDim) / 2
          : (height - axisDim) / 2; // TODO TEST

      int intensity = Emotion.INTENSITYCOUNT -
          1 -
          (max(min((axisPos - offset) / axisDim, 1), 0) *
                  (Emotion.INTENSITYCOUNT - 1))
              .round();

      if (_dragConstants.emotion.intensity != intensity) {
        setState(() {
          _dragConstants.emotion.intensity = intensity;
          if (widget.onChanged != null)
            widget.onChanged(_dragConstants.emotion);
        });
      }
      // var localPos = renderBox.globalToLocal(details.globalPosition);
      // var distFromCenter = (localPos - _boxCenter).distance;
      // todo fill _emotion based on distance
    }
  }

  void _onPanEnd(DragEndDetails details) {
    if (_dragConstants.emotion.intensity != null)
      widget.onValueSet(
          Emotion.from(_dragConstants.emotion)); //todo set state here too? !!!
    else
      widget.onValueSet(null);
    _dragConstants = null;
  }

  int _getEmotion(Offset offset) {
    // subtract from center
    Offset fromCenter = _boxCenter - offset;

    // find angle based on arctan
    double radAngle =
        atan2(fromCenter.dx, fromCenter.dy); //gives answer between -PI and PI
    radAngle -= EmotionAngleHelper._EMOT_ARC / 2; // offset non-centeredness
    if (radAngle < 0) {
      radAngle += 2 * pi; // get into 0 - 2pi
    }

    // find which of the emotions it falls into
    return (Emotion.EMOTIONCOUNT -
            (radAngle / EmotionAngleHelper._EMOT_ARC).floor() +
            5) %
        8;
  }

  double _getIntensityDistance(double fragmentLen) {
    return _dragConstants != null
        ? (_dragConstants.emotion.intensity != null
            ? ((_dragConstants.emotion.intensity + 0.5) * fragmentLen)
            : 0)
        : null;
  }

  FractionalOffset _getHandleOffset(
      int emotionPos, DragConstants dragConstants) {
    if (dragConstants != null) {
      if (dragConstants.emotion.intensity == null) {
        return FractionalOffset(0.5,
            50); //todo this just hides the handle offscreen; would ideally align it with Cancel
      } else {
        // todo is that what's happening here? split into separate function, but didn't bother to check
        double intensityFraction = -(dragConstants.emotion.intensity -
                (Emotion.INTENSITYCOUNT - 1) / 2) *
            2 /
            (Emotion.INTENSITYCOUNT - 1);
        double x = (cos(EmotionAngleHelper.getEmotionAngleRads(emotionPos)) *
                    intensityFraction) /
                2 +
            0.5;
        double y = (sin(EmotionAngleHelper.getEmotionAngleRads(emotionPos)) *
                    intensityFraction) /
                2 +
            0.5;
        return FractionalOffset(x, y);
      }
    } else {
      //todo cache
      double x =
          cos(EmotionAngleHelper.getEmotionAngleRads(emotionPos)) / 2 + 0.5;
      double y =
          sin(EmotionAngleHelper.getEmotionAngleRads(emotionPos)) / 2 + 0.5;
      return FractionalOffset(x, y);
    }
  }
}

class EmotionHandle extends StatelessWidget {
  IconData icon;
  Color color;

  EmotionHandle(@required IconData this.icon, @required Color this.color);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Icon(
          icon,
          size: 24,
          color: Color(0xff000000),
        ),
        padding: const EdgeInsets.all(12.0),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Color(0x40000000),
              offset: Offset(0.0, 4.0),
              blurRadius: 4.0,
            ),
          ],
          color: color,
        ));
  }
}
