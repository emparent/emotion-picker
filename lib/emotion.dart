// Copyright (c) 2019 Miroslav Mazel
// 
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import 'generated/i18n.dart';
import 'package:flutter/widgets.dart';

class Emotion {
   static List<List<String>> getEmotions(BuildContext context) {
    //todo USE TRANSLATED VERSION!!!
    return
    [[
      I18n.of(context).bored,
      I18n.of(context).apathetic,
      I18n.of(context).repulsed,
      I18n.of(context).bitter,
      I18n.of(context).disgusted,
      I18n.of(context).contemptuous,
      I18n.of(context).loathful
    ],
    [
      I18n.of(context).unfocused,
      I18n.of(context).distracted,
      I18n.of(context).surprised,
      I18n.of(context).amazed,
      I18n.of(context).aghast,
      I18n.of(context).shocked,
      I18n.of(context).awestruck
    ],
    [
      I18n.of(context).vulnerable,
      I18n.of(context).flustered,
      I18n.of(context).embarassed,
      I18n.of(context).inferior,
      I18n.of(context).regretful,
      I18n.of(context).guilty,
      I18n.of(context).ashamed
    ],
    [
      I18n.of(context).pensive,
      I18n.of(context).down,
      I18n.of(context).sad,
      I18n.of(context).mournful,
      I18n.of(context).sorrowful,
      I18n.of(context).miserable,
      I18n.of(context).anguished
    ],
    [
      I18n.of(context).peeved,
      I18n.of(context).irritated,
      I18n.of(context).upset,
      I18n.of(context).angry,
      I18n.of(context).furious,
      I18n.of(context).livid,
      I18n.of(context).enraged
    ],
    [
      I18n.of(context).jittery,
      I18n.of(context).concerned,
      I18n.of(context).nervous,
      I18n.of(context).worried,
      I18n.of(context).scared,
      I18n.of(context).anxious,
      I18n.of(context).terrified
    ],
    [
      I18n.of(context).lively,
      I18n.of(context).interested,
      I18n.of(context).focused,
      I18n.of(context).inspired,
      I18n.of(context).enthusiastic,
      I18n.of(context).excited,
      I18n.of(context).exhilirated
    ],
    [
      I18n.of(context).relaxed,
      I18n.of(context).calm,
      I18n.of(context).pleasant,
      I18n.of(context).content,
      I18n.of(context).joyful,
      I18n.of(context).happy,
      I18n.of(context).blissful
    ]];
   }

  static const int EMOTIONCOUNT = 8;
  static const int INTENSITYCOUNT = 7;

  static Emotion from(Emotion e) {
    return Emotion(emotion: e.emotion, intensity: e.intensity);
  }

  int emotion;
  int intensity;

  Emotion({@required this.emotion, @required this.intensity});
}