// Copyright (c) 2019 Miroslav Mazel
// 
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

import 'dart:math';

import '../emotion.dart';
import '../emotion_display.dart';
import 'package:flutter/widgets.dart';

class Emotions {
  static _getBackgroundGradient(Color center, Color outside) {
    return RadialGradient(
      center: Alignment.center,
      radius: 0.5,
      colors: [
        center,
        outside,
      ],
      stops: [0.5, 1.0],
    );
  }

  static const double _WIDTH = 288;
  static const _GRAY_COLOR = Color(0xFF7F7F7F);
  static final _CENTER_RECT = Rect.fromLTWH(0, 0, _WIDTH, _WIDTH);
  static final _RIGHT_RECT = Rect.fromLTWH(-_WIDTH, 0, _WIDTH * 2, _WIDTH * 2);

  static background(Canvas canvas, Size size) {
    canvas.translate(0.0, 0.0);
    canvas.scale((size.width / _WIDTH), (size.height / _WIDTH));
    final paint = Paint();

    paint.color = Color(0xff000000);
    paint.shader = _getBackgroundGradient(
            Color(EmotionDisplay.basicColors[0]), _GRAY_COLOR)
        .createShader(_CENTER_RECT);
    canvas.drawPath(
        (Path()
          ..moveTo(184.568900, 160.840000)
          ..cubicTo(207.181430, 162.133590, 230.177480, 162.054710, 252.373000,
              157.060690)
          ..cubicTo(263.940580, 154.155950, 275.101440, 149.863460, 285.500000,
              144.000000)
          ..cubicTo(272.595300, 136.604500, 258.110670, 132.770030, 243.635220,
              129.845200)
          ..cubicTo(223.872550, 126.660830, 203.683800, 126.078840, 183.755930,
              127.397150)
          ..cubicTo(170.636250, 132.936760, 157.119680, 138.460380, 144.000000,
              144.000000)
          ..cubicTo(157.347990, 149.589510, 171.063680, 155.329500, 184.429900,
              160.835000)
          ..close()
          ..moveTo(184.688900, 160.636000)
          ..lineTo(184.691900, 160.629000)
          ..close()),
        paint);
    paint.shader = _getBackgroundGradient(
            Color(EmotionDisplay.basicColors[2]), _GRAY_COLOR)
        .createShader(_CENTER_RECT);
    canvas.drawPath(
        (Path()
          ..moveTo(127.210900, 184.156000)
          ..cubicTo(125.917300, 206.768600, 125.996220, 229.764680, 130.990210,
              251.960270)
          ..cubicTo(133.894790, 263.528170, 138.136410, 275.101220, 144.000000,
              285.500000)
          ..cubicTo(151.394540, 272.594780, 155.281150, 257.698920, 158.205710,
              243.223320)
          ..cubicTo(161.391090, 223.460700, 161.972290, 203.271930, 160.653750,
              183.344030)
          ..cubicTo(155.114140, 170.224350, 149.539620, 157.119680, 144.000000,
              144.000000)
          ..cubicTo(138.410490, 157.347650, 132.721400, 170.651110, 127.215900,
              184.017000)
          ..close()
          ..moveTo(127.414900, 184.276000)
          ..lineTo(127.421900, 184.280000)
          ..close()),
        paint);
    paint.shader = _getBackgroundGradient(
            Color(EmotionDisplay.basicColors[4]), _GRAY_COLOR)
        .createShader(_CENTER_RECT);
    canvas.drawPath(
        (Path()
          ..moveTo(103.544900, 127.046000)
          ..cubicTo(80.932288, 125.752420, 57.936181, 125.831260, 35.740580,
              130.825310)
          ..cubicTo(24.172747, 133.729950, 12.898777, 138.136510, 2.500000,
              144.000000)
          ..cubicTo(15.404899, 151.395300, 30.002307, 155.115120, 44.477702,
              158.040320)
          ..cubicTo(64.240233, 161.225510, 84.429043, 161.807060, 104.356870,
              160.488850)
          ..cubicTo(117.476540, 154.948900, 130.880320, 149.539950, 144.000000,
              144.000000)
          ..cubicTo(130.652350, 138.410820, 117.049780, 132.556170, 103.683900,
              127.051000)
          ..close()
          ..moveTo(103.424900, 127.250000)
          ..lineTo(103.420900, 127.257000)
          ..close()),
        paint);
    paint.shader = _getBackgroundGradient(
            Color(EmotionDisplay.basicColors[3]), _GRAY_COLOR)
        .createShader(_CENTER_RECT);
    canvas.drawPath(
        (Path()
          ..moveTo(103.628900, 160.481000)
          ..cubicTo(86.976218, 175.299030, 71.083349, 191.364170, 58.929488,
              210.154170)
          ..cubicTo(52.532596, 220.653920, 47.360041, 231.948850, 44.050800,
              243.802000)
          ..cubicTo(58.407781, 239.910170, 71.468120, 232.322640, 83.778440,
              224.164070)
          ..cubicTo(100.003950, 212.444450, 114.684000, 198.579490, 127.847190,
              183.562330)
          ..cubicTo(133.208100, 170.365220, 138.639100, 157.197110, 144.000000,
              144.000000)
          ..cubicTo(130.609170, 149.486030, 117.075030, 154.828420, 103.729900,
              160.386000)
          ..close()
          ..moveTo(103.687900, 160.710000)
          ..lineTo(103.689900, 160.717000)
          ..close()),
        paint);
    paint.shader = _getBackgroundGradient(
            Color(EmotionDisplay.basicColors[1]), _GRAY_COLOR)
        .createShader(_CENTER_RECT);
    canvas.drawPath(
        (Path()
          ..moveTo(160.645900, 184.319000)
          ..cubicTo(175.464830, 200.970880, 191.528390, 216.865470, 210.319150,
              229.018540)
          ..cubicTo(220.819010, 235.415100, 232.113780, 240.587770, 243.966900,
              243.897000)
          ..cubicTo(240.074900, 229.540380, 232.488370, 216.479750, 224.329570,
              204.169840)
          ..cubicTo(212.610470, 187.943560, 198.744530, 173.264100, 183.727230,
              160.100700)
          ..cubicTo(170.530460, 154.739800, 157.196780, 149.360900, 144.000000,
              144.000000)
          ..cubicTo(149.485690, 157.390830, 154.993640, 170.872880, 160.550900,
              184.218000)
          ..close()
          ..moveTo(160.874900, 184.260000)
          ..lineTo(160.881900, 184.258000)
          ..close()),
        paint);
    paint.shader = _getBackgroundGradient(
            Color(EmotionDisplay.basicColors[7]), _GRAY_COLOR)
        .createShader(_CENTER_RECT);
    canvas.drawPath(
        (Path()
          ..moveTo(184.484900, 127.405000)
          ..cubicTo(201.137010, 112.586800, 217.029640, 96.521833, 229.183440,
              77.732173)
          ..cubicTo(235.580770, 67.232419, 240.752660, 55.937021, 244.062900,
              44.084100)
          ..cubicTo(229.705790, 47.975908, 216.645180, 55.563328, 204.334750,
              63.721959)
          ..cubicTo(188.108810, 75.441230, 173.429740, 89.307343, 160.265610,
              104.323670)
          ..cubicTo(154.904700, 117.520780, 149.360900, 130.802890, 144.000000,
              144.000000)
          ..cubicTo(157.390720, 138.513880, 171.038420, 133.058140, 184.382900,
              127.500000)
          ..close()
          ..moveTo(184.424900, 127.176000)
          ..lineTo(184.422900, 127.169000)
          ..close()),
        paint);
    paint.shader = _getBackgroundGradient(
            Color(EmotionDisplay.basicColors[6]), _GRAY_COLOR)
        .createShader(_CENTER_RECT);
    canvas.drawPath(
        (Path()
          ..moveTo(160.901900, 103.730000)
          ..cubicTo(162.198000, 81.117378, 162.115970, 58.121376, 157.122770,
              35.925680)
          ..cubicTo(154.218940, 24.357580, 149.864200, 12.898437, 144.000000,
              2.500000)
          ..cubicTo(136.605950, 15.405414, 132.833050, 30.187471, 129.907690,
              44.662802)
          ..cubicTo(126.722410, 64.425405, 126.142030, 84.614116, 127.459050,
              104.541970)
          ..cubicTo(132.999000, 117.661640, 138.460050, 130.880320, 144.000000,
              144.000000)
          ..cubicTo(149.589840, 130.652360, 155.390420, 117.234830, 160.897900,
              103.869000)
          ..close()
          ..moveTo(160.697900, 103.610000)
          ..lineTo(160.691900, 103.606000)
          ..close()),
        paint);
    paint.shader = _getBackgroundGradient(
            Color(EmotionDisplay.basicColors[5]), _GRAY_COLOR)
        .createShader(_CENTER_RECT);
    canvas.drawPath(
        (Path()
          ..moveTo(127.466900, 103.567000)
          ..cubicTo(
              112.649440, 86.913931, 96.583802, 71.021742, 77.794273, 58.867488)
          ..cubicTo(
              67.294351, 52.470736, 55.999380, 47.298144, 44.146200, 43.988800)
          ..cubicTo(
              48.038299, 58.345642, 55.625347, 71.406230, 63.784059, 83.716441)
          ..cubicTo(75.503305, 99.942240, 89.368537, 114.621980, 104.385570,
              127.785290)
          ..cubicTo(117.582680, 133.146200, 130.802890, 138.639100, 144.000000,
              144.000000)
          ..cubicTo(138.513970, 130.609170, 133.119480, 117.013130, 127.561900,
              103.668000)
          ..close()
          ..moveTo(127.237900, 103.626000)
          ..lineTo(127.230900, 103.628000)
          ..close()),
        paint);
  }

  static leaf(Canvas canvas, Size size, int emotionIndex) {
    canvas.translate(0.0, 0.0);
    canvas.scale((size.width / _WIDTH), (size.height / _WIDTH));
    final paint = Paint();
    paint.color = Color(0xff000000);

    var angle = pi / 4 * emotionIndex;
    final double r = sqrt(2) * _WIDTH / 2;
    final beta = pi / 4 + angle;
    final shiftY = r * sin(beta);
    final shiftX = r * cos(beta);
    final translateX = _WIDTH / 2 - shiftX;
    final translateY = _WIDTH / 2 - shiftY;
    canvas.translate(translateX, translateY);
    canvas.rotate(angle);

    paint.shader = _getBackgroundGradient(
            Color(EmotionDisplay.basicColors[emotionIndex]), _GRAY_COLOR)
        .createShader(_RIGHT_RECT);
    canvas.drawPath(
        (Path()
          ..moveTo(82.788205, 110.441060)
          ..cubicTo(128.264450, 107.838930, 174.510640, 108.000450, 219.149480,
              118.036420)
          ..cubicTo(242.412770, 123.872270, 265.085300, 132.397350, 285.999780,
              144.182060)
          ..cubicTo(260.043790, 159.044880, 230.688600, 166.853230, 201.578240,
              172.731630)
          ..cubicTo(161.833150, 179.133110, 121.232260, 180.300360, 81.155275,
              177.652640)
          ..cubicTo(54.770255, 166.518780, 28.385225, 155.384920, 2.000215,
              144.251060)
          ..cubicTo(28.843525, 133.017490, 55.628385, 121.516720, 82.508205,
              110.450060)
          ..close()
          ..moveTo(83.030205, 110.851060)
          ..lineTo(83.036205, 110.864060)
          ..close()),
        paint);
  }

  static double getEmotionAngle(int emotionIndex) {
    //todo can save as const array
    return emotionIndex ??
        -(2 * pi / Emotion.EMOTIONCOUNT) *
            (emotionIndex - 1); //todo am I using ?? correctly?
  }
}
